using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Containervervoer1;

namespace Containervervoer2
{
    public class Ship
    {
        private List<ContainerPlacement> _placedContainers = new List<ContainerPlacement>();

        private readonly int _width;
        private readonly int _length;

        public Ship(int width, int length)
        {
            _width = width;
            _length = length;
        }

        public void AddContainers(List<Container> containers)
        {
            IEnumerable<Container> sortedContainers = containers.OrderByDescending(container => container.IsValuable)
                .ThenByDescending(container => container.IsCooled)
                .ThenByDescending(container => container.ContainingWeight);
            bool reverseSearch = false;
            foreach (Container container in sortedContainers)
            {
                try
                {
                    Point3D locationToPlace = FindContainerLocation(container, reverseSearch);
                    PlaceContainer(locationToPlace, container);
                    reverseSearch = !reverseSearch;
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine("The following container could not be added to the ship:");
                    Console.WriteLine("Weight:" + container.ContainingWeight);
                    Console.WriteLine(container);
                }
            }

            List<ContainerPlacement> newStacks = new List<ContainerPlacement>();
            foreach (var point in this.GetGrid())
            {
                var stack = this._placedContainers.Where(x => x.X == point.X && x.Y == point.Y)
                    .OrderByDescending(x => x.Container.IsCooled)
                    .ThenBy(x => x.Container.IsValuable)
                    .ThenByDescending(x => x.Container.ContainingWeight)
                    .ToList();
                for (int i = 0; i < stack.Count(); i++)
                {
                    newStacks.Add(new ContainerPlacement
                    {
                        Container = stack[i].Container,
                        X = stack[i].X,
                        Y = stack[i].Y,
                        Z = i
                    });
                }
            }

            this._placedContainers = newStacks;
        }

        private Point3D FindContainerLocation(Container container, bool reverse)
        {
            IEnumerable<Point> availableLocations = this.GetGrid();

            if (container.IsCooled)
            {
                availableLocations = availableLocations.Where(point => point.Y == this._length - 1);
            }

            if (container.IsValuable)
            {
                availableLocations = availableLocations.Where(point => point.Y == this._length - 1 || point.Y == 0);
            }

            if (reverse)
            {
                availableLocations = availableLocations.OrderByDescending(point => point.X);
            }

            int currentLevel = 0;
            if (_placedContainers.Count > 0)
            {
                currentLevel = _placedContainers.Max(x => x.Z);
            }

            foreach (var availableLocation in availableLocations)
            {
                Point3D location = new Point3D
                {
                    X = availableLocation.X,
                    Y = availableLocation.Y,
                    Z = currentLevel
                };
                if (IsEligible(location, container))
                {
                    return location;
                }
            }

            foreach (var availableLocation in availableLocations)
            {
                Point3D upperLocation = new Point3D
                {
                    X = availableLocation.X,
                    Y = availableLocation.Y,
                    Z = currentLevel + 1
                };
                if (IsEligible(upperLocation, container))
                {
                    return upperLocation;
                }
            }

            throw new InvalidOperationException("There is no available location for this container.");
        }

        //Only checks physics: Is there already a container on this position? Is the current position overloaded?
        private bool IsEligible(Point3D location, Container container)
        {
            if (this._placedContainers.Exists(point3d => point3d.X == location.X && point3d.Y == location.Y && point3d.Z == location.Z))
            {
                return false;
            }

            if (CurrentLocationWeight(new Point(location.X, location.Y)) + container.ContainingWeight > 120000)
            {
                return false;
            }

            return true;
        }

        private int CurrentLocationWeight(Point location)
        {
            return this._placedContainers.Where(placedContainer =>
                    placedContainer.X == location.X && placedContainer.Y == location.Y)
                .Sum(x => x.Container.ContainingWeight);
        }

        private void PlaceContainer(ContainerPlacement containerPlacement)
        {
            this._placedContainers.Add(containerPlacement);
        }

        private List<Point> GetGrid()
        {
            List<Point> grid = new List<Point>();

            for (int i = 0; i < this._width; i++)
            {
                for (int j = 0; j < this._length; j++)
                {
                    grid.Add(new Point
                    {
                        X = i,
                        Y = j
                    });
                }
            }

            return grid;
        }

        private void PlaceContainer(Point3D location, Container container)
        {
            _placedContainers.Add(new ContainerPlacement
            {
                Container = container,
                X = location.X,
                Y = location.Y,
                Z = location.Z
            });
        }

        public override string ToString()
        {
            var shipString = new StringBuilder();

            int shipHeight = _placedContainers.Max(x => x.Z);

            for (int z = 0; z < _placedContainers.Max(x => x.Z); z++)
            {
                for (int x = 0; x < _width; x++)
                {
                    for (int y = 0; y < _length; y++)
                    {
                        var placedContainer = GetPlacedContainerByPosition(new Point3D
                        {
                            X = x,
                            Y = y,
                            Z = z
                        });

                        if (placedContainer == null)
                        {
                            shipString.Append("[ ]");
                        }
                        else
                        {
                            shipString.Append(placedContainer.Container);
                        }
                    }

                    shipString.AppendLine();
                }
                shipString.AppendLine();
                shipString.AppendLine();
            }

            shipString.AppendLine("Final weight:" +
                                  _placedContainers.Sum(x => x.Container.ContainingWeight).ToString());

            return shipString.ToString();
        }

        private ContainerPlacement GetPlacedContainerByPosition(Point3D position)
        {
            return _placedContainers.FirstOrDefault(x => x.X == position.X && x.Y == position.Y && x.Z == position.Z);
        }
    }
}