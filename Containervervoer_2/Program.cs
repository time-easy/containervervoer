﻿using System;
using System.Collections.Generic;

namespace Containervervoer2
{
    class Program
    {
        static void Main(string[] args)
        {
            Ship ship = new Ship(5, 6);
            ship.AddContainers(RandomContainers());
            
            Console.WriteLine(ship);
            
            Console.ReadLine();
        }

        private static List<Container> RandomContainers()
        {
            Random rnd = new Random();
            List<Container> randomContainers = new List<Container>();
            
            int amountOfNormalContainers = rnd.Next(30, 50);

            for (int i = 0; i < amountOfNormalContainers; i++)
            {
                randomContainers.Add(new Container
                {
                    ContainingWeight = rnd.Next(4000, 30000)
                });
            }

            int amountOfCooledContainers = rnd.Next(0, 10);

            for (int i = 0; i < amountOfCooledContainers; i++)
            {
                randomContainers.Add(new Container
                {
                    ContainingWeight = rnd.Next(4000, 30000),
                    IsCooled = true
                });
            }
            
            int amountOfValuableContainers = rnd.Next(10, 20);

            for (int i = 0; i < amountOfCooledContainers; i++)
            {
                randomContainers.Add(new Container
                {
                    ContainingWeight = rnd.Next(4000, 30000),
                    IsValuable = true
                });
            }

            return randomContainers;
        } 
    }
}