namespace Containervervoer2
{
    public class Container
    {
        public int ContainingWeight { get; set; }
        
        public bool IsValuable { get; set; }
        
        public bool IsCooled { get; set; }

        public override string ToString()
        {
            if (this.IsValuable)
            {
                return "[V]";
            }
            else if (this.IsCooled)
            {
                return "[C]";
            }
            else
            {
                return "[N]";
            }
        }
    }
}