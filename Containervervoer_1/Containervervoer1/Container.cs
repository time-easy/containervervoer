namespace Containervervoer1
{
    public class Container
    {
        public int ContainingWeight { get; set; }
        
        public bool IsValuable { get; set; }
        
        public bool IsCooled { get; set; }
    }
}