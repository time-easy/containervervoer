using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Containervervoer1
{
    public class Ship
    {
        private readonly List<ContainerPlacement> _placedContainers = new List<ContainerPlacement>();

        private readonly int _width;
        private readonly int _length;

        public Ship(int width, int length)
        {
            _width = width;
            _length = length;
        }

        public void AddContainers(List<Container> containers)
        {
            IEnumerable<Container> allContainers = containers.OrderByDescending(x => x.ContainingWeight);
            //1. Place cooled containers in the front row (heavy first)
            int cooledContainerLevel = 0;
            foreach (Container cooledContainer in allContainers.Where(x => x.IsCooled))
            {
                //Only place at the front row (x == 0)
                //If all places are occupied, move one cooledContainerLevel up:
                if (this.GetGrid().TrueForAll(x => this.IsOccupied(new Point3D
                {
                    X = 0,
                    Y = x.Y,
                    Z = cooledContainerLevel
                })))
                {
                    cooledContainerLevel++;
                }

                //Find the first point where a Container may be placed
                Point pointToPlaceContainer = this.GetGrid().First(x => !this.IsOccupied(new Point3D
                {
                    X = 0,
                    Y = x.Y,
                    Z = cooledContainerLevel
                }));

                //Place the container on the found point
                this.PlaceContainer(new ContainerPlacement
                {
                    Container = cooledContainer,
                    X = pointToPlaceContainer.X,
                    Y = pointToPlaceContainer.Y,
                    Z = cooledContainerLevel
                });
            }

            //2. Place normal containers (heavy first)
            bool leftToRight = true;
            int normalContainerLevel = 0;
            foreach (Container normalContainer in allContainers.Where(x => !x.IsCooled && !x.IsValuable))
            {
                //Find a spot to place container (start with left to right)
                var tiles = this.GetGrid();
                if (leftToRight == false)
                {
                    //If the previous spot was found using left-to-right searching, then now search using right-to-left
                    tiles.Reverse();
                }

                
                if (tiles.All(x => !this.IsNormalContainerPlaceable(new Point3D
                {
                    X = x.X,
                    Y = x.Y,
                    Z = normalContainerLevel
                })))
                {
                    //If all tiles on the current level are occupied, move one level up
                    normalContainerLevel += 1;
                }

                //Find the point where to place the container
                Point pointToPlaceContainer = tiles.First(x => this.IsNormalContainerPlaceable(new Point3D
                {
                    X = x.X,
                    Y = x.Y,
                    Z = normalContainerLevel
                }));
                this.PlaceContainer(new Point3D
                    {
                        X = pointToPlaceContainer.X,
                        Y = pointToPlaceContainer.Y,
                        Z = normalContainerLevel
                    },
                    normalContainer);
                leftToRight = !leftToRight;
            }
            //3. Place valuable containers

            foreach (Container container in containers.Where(x => x.IsValuable))
            {
                
            }
        }

        private void PlaceContainer(ContainerPlacement containerPlacement)
        {
            this._placedContainers.Add(containerPlacement);
        }

        private List<Point> GetGrid()
        {
            List<Point> grid = new List<Point>();

            for (int i = 0; i < this._width; i++)
            {
                for (int j = 0; j < this._length; j++)
                {
                    grid.Add(new Point
                    {
                        X = j,
                        Y = i
                    });
                }
            }

            return grid;
        }

        private bool IsOccupied(Point3D location)
        {
            return this._placedContainers.Any(x => x.X == location.X && x.Y == location.Y && x.Z == location.Z);
        }

        private bool IsNormalContainerPlaceable(Point3D location)
        {
            if (IsOccupied(location))
            {
                return false;
            }

            if (location.Z == 0)
            {
                return true;
            }

            Point3D locationUnderneath = new Point3D
            {
                X = location.X,
                Y = location.Y,
                Z = location.Z - 1
            };

            if (!IsOccupied(locationUnderneath))
            {
                return false;
            }

            Point3D locationUnderneathLeft = new Point3D
            {
                X = locationUnderneath.X - 1,
                Y = locationUnderneath.Y,
                Z = locationUnderneath.Z
            };

            Point3D locationUnderneathRight = new Point3D
            {
                X = locationUnderneath.X + 1,
                Y = locationUnderneath.Y,
                Z = locationUnderneath.Z
            };

            //Check if the container underneath is accessible from only one side.
            if (IsOccupied(locationUnderneathLeft) != IsOccupied(locationUnderneathRight))
            {
                return false;
            }

            return true;
        }

        private void PlaceContainer(Point3D location, Container container)
        {
            _placedContainers.Add(new ContainerPlacement
            {
                Container = container,
                X = location.X,
                Y = location.Y,
                Z = location.Z
            });
            //place container on location
        }

        public override string ToString()
        {
            int level = this._placedContainers.OrderByDescending(x => x.Z).First().Z + 1;

            StringBuilder shipString = new StringBuilder();

            for (int i = level - 1; i >= 0; i--)
            {
                foreach (var point in this.GetGrid())
                {
                    Point3D point3D = new Point3D
                    {
                        X = point.X,
                        Y = point.Y,
                        Z = i
                    };
                    if (IsOccupied(point3D))
                    {
                        ContainerPlacement placedContainer = GetPlacedContainerByPosition(point3D);
                        if (placedContainer.Container.IsCooled)
                        {
                            shipString.Append("[C]");
                        } else if (placedContainer.Container.IsValuable)
                        {
                            shipString.Append("[V]");
                        }
                        else
                        {
                            shipString.Append("[N]");
                        }
                    }
                    else
                    {
                        shipString.Append("[ ]");
                    }
                    
                }
                shipString.AppendLine();
            }

            return shipString.ToString();
        }

        private ContainerPlacement GetPlacedContainerByPosition(Point3D position)
        {
            return _placedContainers.FirstOrDefault(x => x.X == position.X && x.Y == position.Y && x.Z == position.Z);
        }
    }
}